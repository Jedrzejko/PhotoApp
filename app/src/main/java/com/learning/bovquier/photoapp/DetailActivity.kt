package com.learning.bovquier.photoapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.learning.bovquier.photoapp.models.Photo
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setSupportActionBar(toolbar)
        val photo = intent.getSerializableExtra(KEY_PHOTO) as Photo?
        photo?.webformatURL.let {
            Glide.with(this).load(photo?.webformatURL).into(ivDetailedPhoto)
        }
        ivDetailedPhoto.setOnClickListener {
            finish()
        }
    }

    companion object {
        val KEY_PHOTO = "KEY_PHOTO"
    }
}
