package com.learning.bovquier.photoapp

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.learning.bovquier.photoapp.api.PhotoRetriever
import com.learning.bovquier.photoapp.models.PhotoList
import com.learning.bovquier.photoapp.models.PhotoViewHolder
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var mainAdapter: MainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        rvPhotos.layoutManager = LinearLayoutManager(this)
        val photoRetriever = PhotoRetriever()
        val callback = object : Callback<PhotoList> {
            override fun onFailure(call: Call<PhotoList>?, t: Throwable?) {
                Log.e(MainActivity::class.simpleName, "Error while loading data", t)
            }

            override fun onResponse(call: Call<PhotoList>?, response: Response<PhotoList>?) {
                response.let {
                    mainAdapter = MainAdapter(it?.body()?.hits!!, this@MainActivity)
                    rvPhotos.adapter = mainAdapter
                }
            }

        }
        photoRetriever.getPhotos(callback)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onClick(view: View?) {
        val intent = Intent(this, DetailActivity::class.java)
        val holder = view?.tag as PhotoViewHolder
        val photo = mainAdapter.getItem(holder.adapterPosition)
        intent.putExtra(DetailActivity.KEY_PHOTO, photo)
        startActivity(intent)
    }
}
