package com.learning.bovquier.photoapp

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.learning.bovquier.photoapp.models.Photo
import com.learning.bovquier.photoapp.models.PhotoViewHolder

/**
 * @author bovquier
 * on 05.10.2017.
 */
class MainAdapter(private var photos: List<Photo>, var clickListener: View.OnClickListener) :
        RecyclerView.Adapter<PhotoViewHolder>() {
    override fun onBindViewHolder(holder: PhotoViewHolder?, position: Int) {
        holder?.bindValues(photos[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_photo, parent, false)
        view.setOnClickListener(clickListener)
        return PhotoViewHolder(view)
    }

    override fun getItemCount(): Int = photos.size
    fun getItem(adapterPosition: Int): Photo = photos[adapterPosition]
}
