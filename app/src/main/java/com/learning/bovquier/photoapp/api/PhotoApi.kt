package com.learning.bovquier.photoapp.api

import com.learning.bovquier.photoapp.models.PhotoList
import retrofit2.Call
import retrofit2.http.GET

/**
 * @author bovquier
 * on 05.10.2017.
 */
interface PhotoApi {
    @GET("?key=6633973-705050361e73afa591fac14ad&q=nature&image_type=photo")
    fun getPhotos() : Call<PhotoList>
}