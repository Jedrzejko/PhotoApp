package com.learning.bovquier.photoapp.models

import java.io.Serializable

/**
 * @author bovquier
 * on 05.10.2017.
 */
data class Photo(val id: String, val likes: Int, val favorites: Int, val tags: String, val previewURL:
String, val webformatURL: String) : Serializable