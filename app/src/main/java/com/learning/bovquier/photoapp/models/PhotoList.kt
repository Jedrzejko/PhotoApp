package com.learning.bovquier.photoapp.models

import java.io.Serializable

/**
 * @author bovquier
 * on 05.10.2017.
 */
data class PhotoList(val hits: List<Photo>) : Serializable