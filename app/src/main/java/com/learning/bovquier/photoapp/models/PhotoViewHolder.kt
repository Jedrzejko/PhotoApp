package com.learning.bovquier.photoapp.models

import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_photo.view.*

/**
 * @author bovquier
 * on 05.10.2017.
 */
class PhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    init {
        itemView.tag = this
    }

    fun bindValues(item: Photo) {
        itemView.tvFavorites.text = item.favorites.toString()
        itemView.tvLikes.text = item.likes.toString()
        itemView.tvTags.text = item.tags
        if (item.previewURL.isNotBlank()) {
            Glide.with(itemView.context).load(item.previewURL).into(itemView.ivPhoto)
        }
    }
}